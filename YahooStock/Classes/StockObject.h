//
//  StockObject.h
//  YahooStock
//
//  Created by Александр Андрюхин on 26.03.16.
//  Copyright © 2016 AlexanderAndrukhin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StockObject : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *priceString;
@property (nonatomic, strong) NSString *dynamicString;

@property (nonatomic, strong) NSString *closeString;
@property (nonatomic, strong) NSString *hightString;
@property (nonatomic, strong) NSString *lowString;
@property (nonatomic, assign) BOOL isPositiveDynamic;

- (instancetype)initWithResponse:(NSDictionary *)response;

@end
