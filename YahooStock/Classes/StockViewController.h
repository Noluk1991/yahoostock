//
//  StockTableViewController.h
//  YahooStock
//
//  Created by Александр Андрюхин on 26.03.16.
//  Copyright © 2016 AlexanderAndrukhin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIScrollView+EmptyDataSet.h"

@interface StockViewController : UIViewController  <UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating, UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>

@end
