//
//  YahooResponseSerializer.m
//  YahooStock
//
//  Created by Александр Андрюхин on 26.03.16.
//  Copyright © 2016 AlexanderAndrukhin. All rights reserved.
//

#import "YahooResponseSerializer.h"

@implementation YahooResponseSerializer

- (id)responseObjectForResponse:(NSURLResponse *)response
                           data:(NSData *)data
                          error:(NSError *__autoreleasing *)error {
    
    NSDictionary *jsonResponse = nil;
    if (data) {
        NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSRange range = [jsonString rangeOfString:@"("];
        range.location++;
        range.length = [jsonString length] - range.location - 2; // removes parens and trailing semicolon
        jsonString = [jsonString substringWithRange:range];
        
        NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        NSError *jsonError = nil;
        jsonResponse = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&jsonError];
    } else {
        NSLog(@"Error loading data: %@", *error );
    }
    return jsonResponse;
}

@end
