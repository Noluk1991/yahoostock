//
//  InfoFetcher.h
//  YahooStock
//
//  Created by Александр Андрюхин on 26.03.16.
//  Copyright © 2016 AlexanderAndrukhin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InfoFetcher : NSObject

+ (id)sharedFetcher;
- (void)lookupStockInfoWithSearchString:(NSString *)searchString completion:(void(^)(NSArray *stocks))completion;

@end
