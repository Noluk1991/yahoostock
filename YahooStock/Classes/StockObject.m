//
//  StockObject.m
//  YahooStock
//
//  Created by Александр Андрюхин on 26.03.16.
//  Copyright © 2016 AlexanderAndrukhin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StockObject.h"

@implementation StockObject

- (instancetype)initWithResponse:(NSDictionary *)response {
    self = [super init];
    
    if (self) {
        
       response = [self checkResponse:response];
        
        _name = response[@"Symbol"];
        _dynamicString = response[@"Change"];
        _priceString = response[@"LastTradePriceOnly"];

        _closeString = response[@"PreviousClose"];
        _hightString = response[@"DaysHigh"];
        _lowString = response[@"DaysLow"];
        
        CGFloat dynamicValue = [_dynamicString floatValue];
        _isPositiveDynamic = dynamicValue > 0 ? YES: NO;
    }
    return self;
}

- (NSDictionary *)checkResponse:(NSDictionary* )response {
    NSMutableDictionary *mutableResponse = [response mutableCopy];
    for (NSString *key in response.allKeys) {
        if ([response[key] isKindOfClass:[NSNull class]]) {
            mutableResponse[key] = @"--";
        }
    }
    return mutableResponse;
}

@end
