//
//  StockTableViewController.m
//  YahooStock
//
//  Created by Александр Андрюхин on 26.03.16.
//  Copyright © 2016 AlexanderAndrukhin. All rights reserved.
//

#import "StockViewController.h"
#import "InfoFetcher.h"
#import "StockTableViewCell.h"
#import "StockObject.h"

@interface StockViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *stocks;
@property (nonatomic, strong) NSArray *filteredStocks;
@property (strong, nonatomic) UIActivityIndicatorView *indicator;
@property (nonatomic, strong) UISearchController *searchController;

@end

@implementation StockViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.indicator.frame = self.view.frame;
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    [self.view addSubview:self.indicator];
    [self setupSearchController];
    [self loadStocksWithSearchString:nil];
}

- (void)loadStocksWithSearchString:(NSString *)searchString {
    __weak StockViewController *weakSelf = self;
    [self.indicator startAnimating];
    [[InfoFetcher sharedFetcher] lookupStockInfoWithSearchString:searchString completion:^(NSArray *stocks) {
        weakSelf.stocks = stocks;
        [weakSelf.indicator stopAnimating];
        [weakSelf.tableView reloadData];
    }];
}

#pragma mark - setup

- (void)setupSearchController {
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.searchBar.barTintColor = [UIColor blackColor];
    self.tableView.tableHeaderView = self.searchController.searchBar;
    
    self.searchController.delegate = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.searchBar.delegate = self;
    self.definesPresentationContext = YES;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.stocks.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *stockCellIdentifier = @"Cell";
    
    StockTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:stockCellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[StockTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:stockCellIdentifier];
    }
    
    StockObject *stock = [self.stocks objectAtIndex:indexPath.row];
    cell.nameLabel.text = stock.name;
    cell.priceLabel.text = stock.priceString;
    cell.dynamicLabel.text = stock.dynamicString;
    cell.dynamicLabel.textColor = stock.isPositiveDynamic ? [UIColor greenColor]: [UIColor redColor];
    
    cell.closePriceLabel.text = stock.closeString;
    cell.hightPriceLabel.text = stock.hightString;
    cell.lowPriceLabel.text = stock.lowString;
 
    return cell;
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    [self loadStocksWithSearchString:searchBar.text];

}

#pragma mark - UISearchResultsUpdating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    
}

#pragma mark - DZNEmptyDataSetSource

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {
    NSString *text = @"No results";
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0f],
                                 NSForegroundColorAttributeName: [UIColor whiteColor]};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state {
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:17.0],
                                 NSForegroundColorAttributeName: [UIColor greenColor]};
    
    return [[NSAttributedString alloc] initWithString:@"Load defaults values" attributes:attributes];
}

- (void)emptyDataSetDidTapButton:(UIScrollView *)scrollView {
    [self loadStocksWithSearchString:nil];
}

@end
