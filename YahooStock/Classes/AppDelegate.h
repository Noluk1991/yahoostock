//
//  AppDelegate.h
//  YahooStock
//
//  Created by Александр Андрюхин on 26.03.16.
//  Copyright © 2016 AlexanderAndrukhin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

