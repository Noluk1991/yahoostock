//
//  StockTableViewCell.m
//  YahooStock
//
//  Created by Александр Андрюхин on 26.03.16.
//  Copyright © 2016 AlexanderAndrukhin. All rights reserved.
//

#import "StockTableViewCell.h"

@implementation StockTableViewCell

- (void)awakeFromNib {
    self.nameLabel.adjustsFontSizeToFitWidth = YES;
    self.priceLabel.adjustsFontSizeToFitWidth = YES;
    self.dynamicLabel.adjustsFontSizeToFitWidth = YES;
    self.closePriceLabel.adjustsFontSizeToFitWidth = YES;
    self.hightPriceLabel.adjustsFontSizeToFitWidth = YES;
    self.lowPriceLabel.adjustsFontSizeToFitWidth = YES;
}

@end
