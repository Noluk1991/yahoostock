//
//  InfoFetcher.m
//  YahooStock
//
//  Created by Александр Андрюхин on 26.03.16.
//  Copyright © 2016 AlexanderAndrukhin. All rights reserved.
//

#import "InfoFetcher.h"
#import "YahooResponseSerializer.h"
#import <AFNetworking/AFNetworking.h>
#import "StockObject.h"

@implementation InfoFetcher

+ (id)sharedFetcher {
    static InfoFetcher *sharedFetcher = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedFetcher = [[self alloc] init];
    });
    return sharedFetcher;
}

- (void)fetchStockInfoForCompanies:(NSArray *)companies completion:(void(^)(NSArray *stocks))completion {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSString *symbolString = @"'YHOO','AAPL','GOOG','INTC','TWTR','FB','NFLX','AMZN','LNKD'";
    if (companies) {
        symbolString = [[companies valueForKey:@"description"] componentsJoinedByString:@","];
    }
    
    NSString *urlString = [NSString stringWithFormat:@"https://query.yahooapis.com/v1/public/yql?q=select+*+from+yahoo.finance.quotes+where+symbol+in+(%@)&format=json&diagnostics=true&env=store://datatables.org/alltableswithkeys&callback=", symbolString];
    
    [manager GET:urlString parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        __unused NSHTTPURLResponse* r = (NSHTTPURLResponse*)task.response;
        //NSLog( @"success status code: %ld", (long)r.statusCode ); // для статус кода
        //NSLog(@"response object %@", responseObject); // уже NSDictionary
        NSMutableArray *stocks = [NSMutableArray new];
        NSDictionary *resultsDictionary = [[responseObject objectForKey:@"query"] objectForKey:@"results"];
        NSArray *companiesInfoArray = [resultsDictionary objectForKey:@"quote"];
        for (NSDictionary *companyInfo in companiesInfoArray) {
            if (![companyInfo[@"LastTradePriceOnly"] isKindOfClass:[NSNull class]]) {
                StockObject *stock = [[StockObject alloc]initWithResponse:companyInfo];
                [stocks addObject:stock];
            }
        }
        completion(stocks);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        //NSHTTPURLResponse* r = (NSHTTPURLResponse*)task.response;
       // NSLog( @"failure status code: %ld", (long)r.statusCode );
        //NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
        //NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
        //NSLog(@"Failure error serialised - %@",serializedData);
        completion(nil);
    }];
}


- (void)lookupStockInfoWithSearchString:(NSString *)searchString completion:(void(^)(NSArray *stocks))completion {
    __weak InfoFetcher *weakSelf = self;
    
    searchString = [self removeSpacesFromString:searchString];
    if (([searchString isEqualToString:@""]) || (searchString == nil)) {
        [self fetchStockInfoForCompanies:nil completion:^(NSArray *stocks) {
            completion(stocks);
        }];
        return;
    }
    NSString *lookupUrl = [NSString stringWithFormat: @"http://d.yimg.com/aq/autoc?query=%@&region=US&lang=en-US&callback=YAHOO.util.ScriptNodeDataSource.callbacks", searchString];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    YahooResponseSerializer *serializer = [YahooResponseSerializer serializer];
    serializer.acceptableContentTypes = [NSSet setWithObject:@"application/javascript"];
    manager.responseSerializer = serializer;
    
    [manager GET:lookupUrl parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSArray *results = [[responseObject objectForKey:@"ResultSet"] objectForKey:@"Result"];
        NSMutableArray *symbols = [[NSMutableArray alloc]init];
        for (NSDictionary *resultDictionary in results) {
            [symbols addObject:[NSString stringWithFormat:@"'%@'", [resultDictionary objectForKey:@"symbol"]]];
        }
        NSMutableArray *searchSymbols = [symbols mutableCopy];
        for (NSString *symbol in symbols) {
            if ([symbol containsString:@"^"]) {
               [searchSymbols removeObjectIdenticalTo:symbol];
            }
        }
        [weakSelf fetchStockInfoForCompanies:searchSymbols completion:^(NSArray *stocks) {
            completion(stocks);
        }];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        //NSLog(@"failure %@", error.description);
    }];
}

- (NSString *)removeSpacesFromString:(NSString *)string {
    NSArray *words = [string componentsSeparatedByCharactersInSet :[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *stringWithoutSpaces = [words componentsJoinedByString:@"+"];
    NSString *encodedPrefix= [stringWithoutSpaces stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    return encodedPrefix;
}

@end
