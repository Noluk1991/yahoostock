//
//  YahooResponseSerializer.h
//  YahooStock
//
//  Created by Александр Андрюхин on 26.03.16.
//  Copyright © 2016 AlexanderAndrukhin. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

@interface YahooResponseSerializer : AFJSONResponseSerializer

@end
