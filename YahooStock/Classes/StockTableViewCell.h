//
//  StockTableViewCell.h
//  YahooStock
//
//  Created by Александр Андрюхин on 26.03.16.
//  Copyright © 2016 AlexanderAndrukhin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StockTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *dynamicLabel;

@property (weak, nonatomic) IBOutlet UILabel *closePriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *hightPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *lowPriceLabel;

@end
